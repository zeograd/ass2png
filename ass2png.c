/*
 * Copyright (C) 2006 Evgeniy Stepanov <eugeni.stepanov@gmail.com>
 * Copyright (C) 2009 Grigori Goronzy <greg@geekmind.org>
 * Copyright (C) 2016 Olivier Jolly <zeograd@gmail.com>
 *
 * Adaptation of the example from libass.
 *
 * Permission to use, copy, modify, and distribute this software for any
 * purpose with or without fee is hereby granted, provided that the above
 * copyright notice and this permission notice appear in all copies.
 *
 * THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIES
 * WITH REGARD TO THIS SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OF
 * MERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR
 * ANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES
 * WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN AN
 * ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OF
 * OR IN CONNECTION WITH THE USE OR PERFORMANCE OF THIS SOFTWARE.
 */

#include <stdlib.h>
#include <ass/ass.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <png.h>

typedef struct image_s {
    int width, height, stride;
    unsigned char *buffer;      // RGBA32
} image_t;

ASS_Library *ass_library;
ASS_Renderer *ass_renderer;

#pragma clang diagnostic push
#pragma ide diagnostic ignored "ArrayIssues"
char *font_provider_labels[] = {
        [ASS_FONTPROVIDER_NONE]       = "None",
        [ASS_FONTPROVIDER_AUTODETECT] = "Autodetect",
        [ASS_FONTPROVIDER_CORETEXT]   = "CoreText",
        [ASS_FONTPROVIDER_FONTCONFIG] = "Fontconfig",
        [ASS_FONTPROVIDER_DIRECTWRITE]= "DirectWrite",
};
#pragma clang diagnostic pop

/**
 * Callback for libass logging
 * @param level message log level
 * @param fmt message using printf format
 * @param va list of arguments to the message
 * @param data passthrough data from the handler setting
 */
void msg_callback(int level, const char *fmt, va_list va, void *data) {
    if (level > 6)
        return;
    printf("libass: ");
    vprintf(fmt, va);
    printf("\n");
}

/**
 * Init libass
 * @param frame_w target image width
 * @param frame_h target image height
 */
static void init(int frame_w, int frame_h) {
    ass_library = ass_library_init();
    if (!ass_library) {
        printf("ass_library_init failed!\n");
        exit(1);
    }

    ass_set_message_cb(ass_library, msg_callback, NULL);

    ass_renderer = ass_renderer_init(ass_library);
    if (!ass_renderer) {
        printf("ass_renderer_init failed!\n");
        exit(1);
    }

    ass_set_frame_size(ass_renderer, frame_w, frame_h);
    ass_set_fonts(ass_renderer, NULL, "sans-serif",
                  ASS_FONTPROVIDER_AUTODETECT, NULL, 1);
}

/**
 * Display libass font providers
 * @param ass_library libass library reference
 */
static void print_font_providers(ASS_Library *ass_library) {
    int i;
    ASS_DefaultFontProvider *providers;
    size_t providers_size = 0;
    ass_get_available_font_providers(ass_library, &providers, &providers_size);
    printf("test.c: Available font providers (%zu): ", providers_size);
    for (i = 0; i < providers_size; i++) {
        const char *separator = i > 0 ? ", " : "";
        printf("%s'%s'", separator, font_provider_labels[providers[i]]);
    }
    printf(".\n");
    free(providers);
}

/**
 * Clean the temporary image on which subtitles are blended
 * @param img image to clean
 */
static void clean_image(image_t *img) {
    memset(img->buffer, 0, img->stride * img->height);
}

/**
 * Allocate and clean the image on which subtitles are blended
 * @param width  image width
 * @param height  image height
 * @return allocated image in RGBA32 format
 */
static image_t *gen_image(int width, int height) {
    image_t *img = malloc(sizeof(image_t));
    img->width = width;
    img->height = height;
    img->stride = width * 4;
    img->buffer = (unsigned char *) calloc(1, height * width * 4);
    clean_image(img);
    return img;
}

/**
 * Write an image on disk
 * @param fname filename of the image file to create
 * @param img image content
 */
static void write_png(char *fname, image_t *img) {
    FILE *fp;
    png_structp png_ptr;
    png_infop info_ptr;
    png_byte **row_pointers;
    int k;

    png_ptr =
            png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    info_ptr = png_create_info_struct(png_ptr);
    fp = NULL;

    if (setjmp(png_jmpbuf(png_ptr))) {
        png_destroy_write_struct(&png_ptr, &info_ptr);
        fclose(fp);
        return;
    }

    fp = fopen(fname, "wb");
    if (fp == NULL) {
        printf("PNG Error opening %s for writing!\n", fname);
        return;
    }

    png_init_io(png_ptr, fp);
    png_set_compression_level(png_ptr, 3);

    png_set_IHDR(png_ptr, info_ptr, img->width, img->height,
                 8, PNG_COLOR_TYPE_RGB_ALPHA, PNG_INTERLACE_NONE,
                 PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

    png_write_info(png_ptr, info_ptr);

    png_set_bgr(png_ptr);

    row_pointers = (png_byte **) malloc(img->height * sizeof(png_byte *));
    for (k = 0; k < img->height; k++)
        row_pointers[k] = img->buffer + img->stride * k;

    png_write_image(png_ptr, row_pointers);
    png_write_end(png_ptr, info_ptr);
    png_destroy_write_struct(&png_ptr, &info_ptr);

    free(row_pointers);

    fclose(fp);

//    unsigned error = lodepng_encode32_file(fname, img->buffer, img->width, img->height);
//    if (error) printf("error %u: %s\n", error, lodepng_error_text(error));
}


#define _r(c)  ((c)>>24)
#define _g(c)  (((c)>>16)&0xFF)
#define _b(c)  (((c)>>8)&0xFF)
#define _a(c)  ((c)&0xFF)

/**
 * Blend a subtitle image into the temporary full image
 * @param frame the temporary image to write onto
 * @param img the subtitle subimage to add
 */
static void blend_single(image_t *frame, ASS_Image *img) {
    int x, y;
    float alpha = 255 - _a(img->color);
    unsigned char r = _r(img->color);
    unsigned char g = _g(img->color);
    unsigned char b = _b(img->color);

    unsigned char *src;
    unsigned char *dst;

    // the subtitle image is meant to be blended at (img->dst_x, img->dst_y)
    // using the RGBA color in img->color
    // the img->src bitmap is a 8bits alpha channel multiplied by the color to give the final pixel RGBA

    src = img->bitmap;
    dst = frame->buffer + img->dst_y * frame->stride + img->dst_x * 4;
    for (y = 0; y < img->h; ++y) {
        for (x = 0; x < img->w; ++x) {
            // alpha blending algorithm was a bit tricky to figure out
            // it is an 8bits adaptation of https://en.wikipedia.org/wiki/Alpha_compositing
            float src_alpha = src[x] * alpha / 255.;
            float prod_alpha = dst[x * 4 + 3] + src_alpha / 255. * (255 - dst[x * 4 + 3]);

            float existing_pixel_alpha = (dst[x * 4 + 3] * (255 - src_alpha) / 255.) / prod_alpha;
            float new_pixel_alpha = src_alpha / prod_alpha;

            dst[x * 4 + 0] = existing_pixel_alpha * dst[x * 4 + 0] + new_pixel_alpha * b;
            dst[x * 4 + 1] = existing_pixel_alpha * dst[x * 4 + 1] + new_pixel_alpha * g;
            dst[x * 4 + 2] = existing_pixel_alpha * dst[x * 4 + 2] + new_pixel_alpha * r;
            dst[x * 4 + 3] = prod_alpha;

        }
        src += img->stride;
        dst += frame->stride;
    }
}

/**
 * Blend the subtitle for a complete frame into the temporary full image
 * @param frame the temporary image to write onto
 * @param img the complete subtitle image to add (made of several subimages)
 */
static void blend(image_t *frame, ASS_Image *img) {
    int cnt = 0;
    while (img) {
        blend_single(frame, img);
        ++cnt;
        img = img->next;
    }
}


int main(int argc, char *argv[]) {

    if (argc < 2) {
        fprintf(stderr, "usage: %s <subtitle file> [<fps> [<duration> [<width> [<height> [<image filename prefix>]]]]]\n", argv[0]);
        fprintf(stderr, "  Generate a sequence of PNG files from the ASS subtitle file.\n\n");
        fprintf(stderr, "  <subtitle file> is the mandatory subtitle filename.\n");
        fprintf(stderr, "  <fps> is the associated movie fps (defaults to 30).\n");
        fprintf(stderr, "  <duration> is the associated movie duration in seconds (defaults to 240).\n");
        fprintf(stderr, "  <width> is the associated movie width in pixels (defaults to 1920).\n");
        fprintf(stderr, "  <height> is the associated movie height in pixels (defaults to 1080).\n");
        fprintf(stderr, "  <image filename prefix> is the prefix of generated images, which are appended a sequence number and '.png' (defaults to ass2png-).\n");
        fprintf(stderr, "\n  This crude utility being highly untested, it can randomly cause baldness and mid-air apparition of magrathean sperm whale.\n");
        exit(1);
    }

    char *subfile = argv[1];
    double fps = argc > 2 ? strtod(argv[2], 0) : 30.;
    double duration = argc > 3 ? strtod(argv[3], 0) : 60 * 4;
    int frame_w = argc > 4 ? strtoul(argv[4], NULL, 0) : 1920;
    int frame_h = argc > 5 ? strtoul(argv[5], NULL, 0) : 1080;
    char *imgfile_prefix = argc > 6 ? argv[6] : "ass2png-";

    print_font_providers(ass_library);

    init(frame_w, frame_h);
    ASS_Track *track = ass_read_file(ass_library, subfile, NULL);
    if (!track) {
        printf("track init failed!\n");
        return 1;
    }

    image_t *frame = gen_image(frame_w, frame_h);

    unsigned frame_idx = 0;
    char imgfile[PATH_MAX];

    for (double tm = 0; tm < duration; tm += 1. / fps, ++frame_idx) {

        ASS_Image *img =
                ass_render_frame(ass_renderer, track, (int) (tm * 1000), NULL);
        clean_image(frame);
        blend(frame, img);

        snprintf(imgfile, PATH_MAX, "%s%06d.png", imgfile_prefix, frame_idx);
        write_png(imgfile, frame);

        if (fabs(tm - (frame_idx / fps)) > 0.001) {
            printf("time diff = %03f\n", tm - (frame_idx / fps));
        }
        printf("%.02f/%.02f (%02d%%)\n", tm, duration, (int) (100. * tm / duration));

    }

    free(frame->buffer);
    free(frame);

    ass_free_track(track);
    ass_renderer_done(ass_renderer);
    ass_library_done(ass_library);

    return 0;
}